import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  productList: [],
  productEdit: undefined,
};
const btFormSlice = createSlice({
  name: "btForm",
  initialState,
  reducers: {
    addProduct: (state, action) => {
      state.productList.push(action.payload);
    },
    removeProduct: (state, action) => {
      state.productList = state.productList.filter(
        (prd) => prd.idSV !== action.payload
      );
    },
    editProduct: (state, action) => {
      console.log(action.payload);
      state.productEdit = state.productList.filter(
        (prd) => prd.idSV === action.payload
      );
    },
    updateProduct: (state, action) => {
      let newArr= state.productList.filter( (prd) => prd.idSV !== action.payload.idSV )
      state.productList=[...newArr];
      state.productList.push(action.payload)
      state.productEdit = undefined;
    }
    },
  },
);

export const { actions: btFormActions, reducer: btFormReducer } = btFormSlice;
