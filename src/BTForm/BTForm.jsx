import React from "react";
import ResultTable from "./ResultTable.jsx";
import AddForm from "./AddForm.jsx";

const BTForm = () => {
  return (
    <div>
      <h1 className="">QUẢN LÝ SINH VIÊN</h1>
      <AddForm />
      <ResultTable />
    </div>
  );
};

export default BTForm;
